import { NgModule } from '@angular/core';
import { AddJobModalComponent } from './add-job-modal/add-job-modal';
import { SendBidModalComponent } from './send-bid-modal/send-bid-modal';
@NgModule({
	declarations: [AddJobModalComponent,
    SendBidModalComponent],
	imports: [],
	exports: [AddJobModalComponent,
    SendBidModalComponent]
})
export class ComponentsModule {}
