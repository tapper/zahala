import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {substituteService} from "../../services/substituteService";
import {Config} from "../../services/config";
import {ChatPage} from "../chat/chat";
import {Data} from '../../providers/data';

/**
 * Generated class for the AllSubstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-all-substitute',
    templateUrl: 'all-substitute.html',
})
export class AllSubstitutePage {

    public Substitute: any[] = [];
    public host = '';
    public Type;
    public Area = window.localStorage.area;
    public TotalSub3 = 0;
    public TotalSub4 = 0;
    public Regions: any[] = [];
    SubstituteName: string = '';
    items: any;
    public Id:any;
    public typeName = '';
    public userName;
    public userImage;
    


    constructor(public dataService: Data, public app: App, public navCtrl: NavController, public navParams: NavParams, public substitute: substituteService, public Settings: Config) {
        this.Id = this.navParams.data;
        console.log("Reg : " , Settings.Regions)
        this.Regions = Settings.Regions;

        if (this.Id == 3 || this.Id == 4)
            this.Type = this.Id;
        else
            this.Type = 1;


        this.substitute.getAllSubstitute('getAllSubstitute', this.Id).then((data: any) => {
            this.Substitute = data;
            this.host = Settings.host;
            Settings.Substitute = data.length;

            this.items = this.filterItems(this.SubstituteName);
            console.log("Items : ", this.Substitute)
        });

        this.host = Settings.host;
        this.userName = window.localStorage.name;
        this.userImage = this.host+""+window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);
        console.log("IMAGE : ", this.userImage)
    }

    changeType(type)
    {
        this.Type = type;
    }


    ionViewDidLoad() {
        if (this.Id == 3 || this.Id == 4)
        this.getTotalJobs();
    }

    getTotalJobs() {
        console.log("getTotalJobs")
        this.TotalSub3 = 0;
        this.TotalSub4 = 0;
    
        for (let i = 0; i < this.Substitute.length; i++) {
            if (this.Substitute[i].type_id == 3)
                this.TotalSub3++;
            else
                this.TotalSub4++;
        }
    }

    setFilteredItems() {
        console.log("Filter : ", this.SubstituteName)
        this.items = this.filterItems(this.SubstituteName);
    }

    ionViewWillEnter() {
        //this.Type = this.navParams.data;
        if (this.Id == 3 || this.Id == 4) {
            this.Settings.openJobsType3 = this.TotalSub3;
            this.Settings.openJobsType4 = this.TotalSub4;
        }
    }

    changeTab() {
        //this.navCtrl.parent.select(1);
        console.log("Chat1")

    }

    gotoChat(i) {
        console.log("Chat1 : ", i, this.Substitute[i])
        //this.navCtrl.push(ChatPage, {id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
        this.app.getRootNav().setRoot(ChatPage, {id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
    }

    filterItems(searchTerm) {
        return this.Substitute.filter((item) => {
            return item['name'].toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }

}
