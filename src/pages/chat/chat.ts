import {Component, NgZone, OnInit,ElementRef, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Content} from "ionic-angular";
import {Platform} from 'ionic-angular';
import {Config} from "../../services/config";
import {ChatService} from "../../services/chatService";
import {HomePage} from "../home/home";
import {MessagesPage} from "../messages/messages";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage {
    @ViewChild(Content) content: Content;
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;

    public chatText = '';
    public ChatArray;
    Obj: any;
    public screenHeight = screen.height - 100 + 'px';
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public newdate: any;
    public phpHost;
    innerHeight: any;
    public reciverId: number;
    public reciverName: string;
    public senderId:number = window.localStorage.id;
    public senderName: string = "zila"

    constructor(public navCtrl: NavController, public zone: NgZone, public navParams: NavParams, public ChatService: ChatService, platform: Platform, public Settings: Config, public events: Events) {
        this.ChatService._ChatArray.subscribe(val => {
            this.zone.run(() => {
                this.ChatArray = val;
                console.log("INN" , this.ChatArray)
            });
        });

        this.reciverId = this.navParams.get('id');
        this.reciverName = this.navParams.get('name');
    
        this.ChatService.updateReadMessage('updateReadMessage',  window.localStorage.id, this.reciverId).then((data: any) => {
            console.log("updateReadMessage : ", data)
        });
        
        console.log("C1 : ", this.ChatService.ChatArray);

        this.phpHost = Settings.host;
        document.addEventListener('resume', () => {
            this.getChatDetails();
        });

        events.subscribe('newchat', (user, time) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (this.content._scroll)
                this.scrollBottom();
        });
    }


    ngOnInit() {
        this.getChatDetails();
        this.scrollBottom();
        console.log("C2 : ", this.ChatService.ChatArray);
        this.innerHeight = (window.screen.height);
        let elm = <HTMLElement>document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px'
    }

    scrollBottom() {
        setTimeout(() => {
            var element = document.getElementById("chatContent");
            element.scrollIntoView(true);
            element.scrollTop = element.scrollHeight;
            this.content.scrollToBottom(0);
        }, 300);
    }

    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch(err) { }
    }

    doRefresh(refresher) {
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then((data: any) => {
            console.log("ChatDetails: ", this.ChatService.ChatArray) ;
            this.ChatArray = data;
            this.scrollBottom();
            refresher.complete();
        });
    }


    getChatDetails() {
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then((data: any) => {
            console.log("ChatDetails: ", this.ChatService.ChatArray) ;
            this.ChatArray = data;
            this.scrollBottom();
        });

        //this.ChatArray = data
    }

    ionViewDidLoad() {
        this.scrollBottom();
    }

    ionViewWillEnter() {
        this.scrollBottom();
    }

    addChatTitle() {
        if (this.chatText) {
            let chatText1 = this.chatText;
            this.chatText = ' ';
            this.date = new Date()
            this.hours = this.date.getHours()
            this.minutes = this.date.getMinutes()
            this.seconds = this.date.getSeconds()

            if (this.hours < 10)
                this.hours = "0" + this.hours

            if (this.minutes < 10)
                this.minutes = "0" + this.minutes
            this.time = this.hours + ':' + this.minutes;


            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();

            if (this.dd < 10) {
                this.dd = '0' + this.dd
            }

            if (this.mm < 10) {
                this.mm = '0' + this.mm
            }

            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;
    
           
            this.Obj = {
                id: '',
                sender_id: window.localStorage.id,
                SenderName : window.localStorage.name,
                title: chatText1,
                date: this.newdate,
                reciver_id: this.reciverId,
                SenderImage:window.localStorage.image,
            };
            console.log(this.ChatService.ChatArray)
            this.ChatService.pushToArray(this.Obj);

            this.ChatService.addTitle('addChatTitle', this.Obj).then((data: any) => {
                console.log("Weights : ", data), chatText1 = '', this.content.scrollToBottom();
            });
    
            this.autoSize(40);
        }
    }

    getHour(DateStr) {
        let Hour = DateStr.split(" ");
        return Hour[1];
    }

    enterPress(keyEvent) {
        if (keyEvent.which === 13) {
            this.addChatTitle();
        }
    }

    cutDate(dt)
    {
        dt = dt.split(" ");
        return dt[0];
    }

    goBack() {
        if (this.navParams.get('type') == 1)
            this.navCtrl.push(HomePage, {SelectedIndex: 1});
        else if(this.navParams.get('type') == 2)
            this.navCtrl.push(HomePage, {SelectedIndex:0});
        else if(this.navParams.get('type') == 3)
        this.navCtrl.push(HomePage, {SelectedIndex: 2});
    }
    
    protected autoSize(pixels: number = 0): void {
        // let textArea = this.element.nativeElement.getElementsByTagName('title')[0];
        let textArea = <HTMLElement>document.querySelector(".chatInput")
        textArea.style.overflow = 'hidden';
        textArea.style.height  = 'auto';
        if (pixels === 0){
            textArea.style.height  = textArea.scrollHeight + 'px';
        } else {
            textArea.style.height  = pixels + 'px';
        }
        return;
    }

}
