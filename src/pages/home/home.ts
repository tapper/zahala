import {Component, NgZone} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AllSubstitutePage} from "../all-substitute/all-substitute";
import {MessagesPage} from "../messages/messages";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {Config} from "../../services/config";
import {loginService} from "../../services/loginService";
import {OpenJobsPage} from "../open-jobs/open-jobs";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {JobsService} from "../../../src/services/JobsService";
import {OpenJobs1Page} from "../open-jobs1/open-jobs1";
import { Media, MediaObject } from '@ionic-native/media';
import {ChatService} from "../../services/chatService";


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    public UserType = 0;
    public SelectedIndex:number = 1;
    tab1Root = AllSubstitutePage;
    tab2Root = UserJobsPage;
    tab3Root = MessagesPage;
    tab4Root = OpenJobsPage;
    tab5Root = OpenJobsPage;
    tab6Root = AllSubstitutePage;
    tab7Root = AllSubstitutePage;
    messagesLen:number;
    messages:any;
    

    constructor(public navParams: NavParams ,public zone: NgZone,  private media: Media, public navCtrl: NavController,public ChatService: ChatService, public jobsService:JobsService , public Settings: Config, public Login: loginService, public Server:sent_to_server_service) {
    
        this.ChatService._messagesLen.subscribe(val => {
            this.zone.run(() => {
                this.messagesLen = val;
            });
        });
        
        
        this.Login.getRegions('GetRegions').then(
            (data: any) => {
                console.log("Regions : ", data);
                Settings.Regions = data;
            });

        this.Login.GetTypes('GetTypes').then(
            (data: any) => {
                //console.log("Types : ", data);
                Settings.Types = data;
            });

        if (!Settings.User) {
            this.Login.getUserById('getUserById').then(
                (data: any) => {
                    Settings.User = data[0];
                    console.log("ssss  : " , data)
                    this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
                    this.SelectedIndex = this.UserType == 2 ? 3 : 2;
                    console.log("Jobssss7 : " , data)
                   // console.log(this.UserType + " : "  + this.SelectedIndex)
                });
        }
        else
        {
            this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
            console.log("Jobssss44 : " )
            this.SelectedIndex = 0;//this.navParams.get('SelectedIndex');
            //console.log(this.UserType  + " : "  + this.SelectedIndex)
        }
    
        //this.jobsService.getAllJobs('getAllJobs');
        // this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
        //     console.log("Jobssss1 : " , data)
        // });

        this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
            console.log("Jobssss2 : " , data)
        });
        console.log("CS : " , ChatService.messagesLen);
    
        
        
        //this.getJobs()
        this.getHomeDetails();
    }

    tabClick(tab)
    {
        console.log("Tab : " , tab)
    }

    getHomeDetails()
    {
        this.Server.getHomeDetails('getHomeDetails').then(
            (data: any) => {
                //console.log("getHomeDetails : " , data)
                this.Settings.JobsCount = data.jobs;
                this.Settings.messages = data.messages;
            });
    }

    // getJobs()
    // {
    //     this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
    //         this.jobsService.Jobs = data;
    //         this.Settings.JobsCount = data.length;
    //         console.log("Jobs12 : " ,this.jobsService.Jobs)
    //     });
    // }

}
