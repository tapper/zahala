import { Component } from '@angular/core';
import {ActionSheetController, IonicPage, Loading, LoadingController, NavController, NavParams, Platform, ToastController} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {toastService} from "../../services/toastService";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {TabsPage} from "../tabs/tabs";
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {Diagnostic} from "@ionic-native/diagnostic";
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
    
    public info: Object = {
        id:'',
        name: '',
        phone: '',
        address: '',
        info: '',
        password: '',
        mail: '',
        image: '',
        type: '',
        area: ''
    };
    
    public serverImage = '';
    public Regions: any[] = [];
    public Types: any[] = [];
    public emailregex;
    public base64Image: string;
    lastImage: string = null;
    loading: Loading;
    correctPath;
    currentName;
    public Type:Number;
   
    
    
  constructor(public navCtrl: NavController, public navParams: NavParams ,public Toast: toastService, public Login: loginService) {
      this.Login.getRegions('GetRegions').then(
          (data: any) => {
              console.log("Regions : ", data);
              this.Regions = data;
          });
    
      this.Login.GetTypes('GetTypes').then(
          (data: any) => {
              console.log("Types : ", data);
              this.Types = data;
          });
    
      this.Login.getUserById('getUserById').then(
          (data: any) => {
              console.log("Types : ", data[0]);
              this.info = data[0];
              this.info['area'] = data[0].area_id;
              this.info['type'] = data[0].type_id;
          });
  }
    
    
    doRegister() {
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא')
        else if (this.info['phone'].length < 8)
            this.Toast.presentToast('הכנס מספר טלפון חוקי')
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין')
        }
        else if (this.info['address'].length < 2)
            this.Toast.presentToast('הכנס כתובת חוקית')
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה')
        else if (this.info['area'] == '')
            this.Toast.presentToast('בחר איזור')
        else if (this.info['type'] == '')
            this.Toast.presentToast('בחר סוג תפקיד')
        else {
            this.info['image'] = this.serverImage;
            this.Login.UpdateUser("UpdateUser", this.info).then(data => {
                if (data == 0) {
                    this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    console.log("Response : " , data.name)
                     this.info = data;
                     this.info['area'] = data.area_id;
                     this.info['type'] = data.type_id;
                    
                   
                    window.localStorage.name = this.info['name'];
                    window.localStorage.area = this.info["area_id"];
                    window.localStorage.id = this.info["id"];
                    window.localStorage.name = this.info["name"];
                    window.localStorage.info = this.info["info"];
                    window.localStorage.image = this.info["image"];
                    window.localStorage.type = this.info["type_id"];
                    this.navCtrl.push(HomePage);
                }
            });
        }
    }
    
    
    GoToHome()
    {
        this.navCtrl.push(HomePage);
    }

    
  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

}
