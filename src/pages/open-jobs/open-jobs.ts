import {Component,NgZone,Renderer } from '@angular/core';
import {AlertController, ModalController,IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {SendBidModalComponent} from "../../components/send-bid-modal/send-bid-modal";


/**
 * Generated class for the OpenJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-open-jobs',
    templateUrl: 'open-jobs.html',
})
export class OpenJobsPage{

    public jobs;
    public CurrentJob = '';
    public Notes: any[] = [];
    public Regions: any[] = [];
    public Types: any[] = [];
    public SortType: any[] = [];
    public Area = window.localStorage.area;
    public Type = '';
    public host = ''
    public TotalJob3 = 0;
    public TotalJob4 = 0;
    public userName;
    public userImage;
    public typeName = '';

    constructor(public navCtrl: NavController,public modalCtrl: ModalController, public zone: NgZone, public navParams: NavParams, public jobsService: JobsService, public Settings: Config, public serverService: sent_to_server_service, private alertCtrl: AlertController) {
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        this.host = Settings.host;
        this.userName = window.localStorage.name;
        this.userImage = this.host+""+window.localStorage.image;
        this.typeName = Settings.getTypeName(window.localStorage.type);

        this.jobs = this.jobsService.OpenJobs;
        for(let type of this.Types){
            if(type.type == 2)
                this.SortType.push(type);
        }

        this.jobsService._OpenJobs.subscribe(data => {
            this.zone.run(() => {
                console.log("SubScribe : " , data)
                this.jobs = data;
                this.getTotalJobs();
            });
        });

        console.log("Jobs : " ,this.SortType , this.Regions  )

        this.serverService.getAllNotes('getAllNotes').then((data: any) => {
            this.Notes = data;
        });
    }


    ionViewDidLoad() {
        this.getTotalJobs();
    }

    getTotalJobs()
    {
        this.TotalJob3 = 0;
        this.TotalJob4 = 0;

        for(let i=0;i<this.jobs.length;i++)
        {
            if(this.jobs[i].type == 3)
                this.TotalJob3++;
            else
                this.TotalJob4++;
        }
    }


    ionViewWillEnter()
    {
        this.Type =  this.navParams.data;
        this.Settings.openJobsType3 = this.TotalJob3;
        this.Settings.openJobsType4 = this.TotalJob4;
    }

    doRefresh(refresher) {
        this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
                 this.jobs = data;
                 this.Settings.openJobs = data.length;
                refresher.complete();
        });
    }
    
    reversDate(dt)
    {
        let arr = dt.split('-')
        let str = arr[2] +"/"+arr[1]+"/"+arr[0];
        return str;
    }

    presentPrompt(i) {
        this.CurrentJob = this.jobs[i].id;
        let noteModal = this.modalCtrl.create(SendBidModalComponent);

        noteModal.present();

        noteModal.onDidDismiss(data => {
            if(data.type == 1)
            {
                console.log(data.title + " : " + this.CurrentJob)
                this.serverService.addBid('addBid',this.CurrentJob,data.title ).then((data: any) => {
                    this.jobs[i].isBid = 1
                });
            }
        });
    }
    
    
    cutTime(time)
    {
        let tm = time.split(":");
        return tm[0]+":"+tm[1];
    }
    
    getCurrentDate(date)
    {
        let tm = date.split(" ");
        let tm1 = tm[0].split("-")
        return tm1[2]+"/"+tm1[1]+"/"+tm1[0];
    }
    
    getCurrentTime(date)
    {
        let tm = date.split(" ");
        let tm1 = tm[1].split(":")
        return tm1[0]+":"+tm1[1];
    }

}
