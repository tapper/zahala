import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenJobs1Page } from './open-jobs1';

@NgModule({
  declarations: [
    OpenJobs1Page,
  ],
  imports: [
    IonicPageModule.forChild(OpenJobs1Page),
  ],
})
export class OpenJobs1PageModule {}
