import {Component,NgZone} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";
import {sent_to_server_service} from "../../services/sent_to_server_service";


/**
 * Generated class for the OpenJobs1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-open-jobs1',
  templateUrl: 'open-jobs1.html',
})
export class OpenJobs1Page {

    public jobs;
    public CurrentJob = '';
    public Notes: any[] = [];
    public Regions: any[] = [];
    public Types: any[] = [];
    public SortType: any[] = [];
    public Area = '';
    public Type = '';
    public host = ''

    constructor(public navCtrl: NavController,public zone: NgZone, public navParams: NavParams, public jobsService: JobsService, public Settings: Config, public serverService: sent_to_server_service, private alertCtrl: AlertController) {
        this.Regions = Settings.Regions;
        this.Types = Settings.Types;
        this.host = Settings.host;
        this.jobs = this.jobsService.OpenJobs;

        for(let type of this.Types){
            if(type.type == 2)
                this.SortType.push(type);
        }

        this.jobsService._OpenJobs.subscribe(data => {
            this.zone.run(() => {
                console.log("SubScribe : " , data)
                this.jobs = data;
            });
        });

        console.log("Jobs : " ,this.SortType , this.Regions  )

        // this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
        //     this.jobs = data;
        //     Settings.openJobs = data.length;
        // });

        this.serverService.getAllNotes('getAllNotes').then((data: any) => {
            this.Notes = data;
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad OpenJobsPage');
    }

    presentPrompt(i) {
        console.log("JB " , i , this.jobs[i].id)
        this.CurrentJob = this.jobs[i].id;
        let myAlert = this.alertCtrl.create(
            {
                title: "בחרי תבנית",
                cssClass : 'noteAlert',
                buttons: [
                    {
                        text: 'בטלי',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'הגישי הצעה',
                        handler: (data) => {
                            console.log("J1 : " , this.CurrentJob ,this.Notes[data].id )
                            this.serverService.addBid('addBid',this.CurrentJob,this.Notes[data].id ).then((data: any) => {
                                this.jobs[i].isBid = 1
                            });
                        }
                    }
                ]
            }
        );

        for (let i = 0; i < this.Notes.length; i++) {
            myAlert.addInput({
                type: 'radio',
                label: this.Notes[i]['title'],
                value: i.toString()
            })
        }
        myAlert.present();
    }

}
