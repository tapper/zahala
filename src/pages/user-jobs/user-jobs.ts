import {Component , OnInit , NgZone} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AddJobModalComponent} from "../../components/add-job-modal/add-job-modal";
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";
import {ProjectBidsPage} from "../project-bids/project-bids";
import { App } from 'ionic-angular';
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {AddJobPage} from "../add-job/add-job";

/**
 * Generated class for the UserJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user-jobs',
    templateUrl: 'user-jobs.html',
})
export class UserJobsPage implements OnInit{

    public jobs:any[] = [];
    public host;

    constructor(private app: App ,public zone: NgZone,  public navCtrl: NavController, public navParams: NavParams , public modalCtrl: ModalController, public jobsService:JobsService , public Settings:Config, public serverService:sent_to_server_service) {
        console.log("SubScribe22 : " )
        this.jobs = this.jobsService.Jobs;
        this.host = Settings.host;

        this.jobsService._Jobs.subscribe(data => {
            this.zone.run(() => {
                console.log("SubScribe : " , data)
                this.jobs = data;
            });
        });
    }

    ionViewWillEnter()
    {
        console.log("ionViewWillEnterJobssss2")
        this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
            this.jobs = data;
        });
    }

    ngOnInit()
    {

    }

    doRefresh(refresher) {
        this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
            this.jobs = data;
            refresher.complete();
        });
    }

    openModal() {
        //this.app.getRootNav().setRoot(AddJobPage);
        this.navCtrl.push(AddJobPage);
        // let jobsModal = this.modalCtrl.create(AddJobModalComponent);
        // jobsModal.present();
        //
        // jobsModal.onDidDismiss(data => {
        //     console.log("jb : " , data);
        //     if(data.type == 1)
        //     {
        //         console.log("send1")
        //         this.jobsService.addJob('addJob',data).then((data: any) => {
        //             console.log("send2 : " + data);
        //             //alert(JSON.stringify(data));
        //             this.jobs = data;
        //         });
        //     }
        // });
    }

    updateModal(i) {
        let noteModal = this.modalCtrl.create(AddJobModalComponent, {
            job:this.jobs[i]
        });

        noteModal.present();

        noteModal.onDidDismiss(data => {
            console.log(data);
             this.serverService.updateJob('updateJob', data,this.jobs[i]['id']).then((data: any) => {
                 this.jobs = data;
             });
        });
    }


    open_close_project(job)
    {
        job.is_close == 0 ?  job.is_close = 1 :  job.is_close = 0;

        this.jobsService.UpdateJobState('UpdateJobState',job.id,job.is_close).then((data: any) => {
            console.log("JobState : " , data)
        });
    }

    checkState(type)
    {
        let str = (type == 0) ? 'לביטול הפרוייקט' : 'לפתיחת הפרוייקט';
        return str;
    }

    open_project_bid(i)
    {
        this.app.getRootNav().setRoot(ProjectBidsPage,{project:this.jobs[i].bids});
        //this.navCtrl.push(ProjectBidsPage,{project:this.jobs[i].bids});
    }
    
    reversDate(dt)
    {
        let arr = dt.split('-')
        let str = arr[2] +"/"+arr[1]+"/"+arr[0];
        return str;
    }
    
    cutTime(time)
    {
        let tm = time.split(":");
        return tm[0]+":"+tm[1];
    }
    
    getCurrentDate(date)
    {
        let tm = date.split(" ");
        let tm1 = tm[0].split("-")
        return tm1[2]+"/"+tm1[1]+"/"+tm1[0];
    }
    
    getCurrentTime(date)
    {
        let tm = date.split(" ");
        let tm1 = tm[1].split(":")
        return tm1[0]+":"+tm1[1];
    }

}
