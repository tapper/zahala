import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterJobs',
})
export class FilterJobsPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value:any,region:number) {
      console.log("Val " , value)
      if(!value)
          return "";
      if(value.length == 0)
      return value;
      console.log("Len : " , value[0] , region );
      let resultArray:any[] = [];

      for(const item of value)
      {
          if(item.region == region || item.area_id == region)
          {
              resultArray.push(item);
          }
      }
      
      return resultArray;
  }
}
