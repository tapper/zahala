import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {Injectable, OnInit} from "@angular/core";
import {HttpModule} from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";


@Injectable()

export class JobsService implements OnInit {
    public ServerUrl;
    Jobs: any[] = [];
    public _Jobs = new Subject<any>();
    Jobs$: Observable<any> = this._Jobs.asObservable();

    OpenJobs: any[] = [];
    public _OpenJobs = new Subject<any>();
    OpenJobs$: Observable<any> = this._OpenJobs.asObservable();

    constructor(private http: Http, public Settings: Config) {
        this.ServerUrl = Settings.ServerUrl;
    };

    ngOnInit() {
        console.log("INIT")
    }

    pushJob(bid) {

        this.getBidById('getBidById', bid).then((data: any) => {
            //alert(JSON.stringify(this.Jobs) + " : length : " + this.Jobs.length )
            for(let i=0;i<this.Jobs.length; i++)
            {
                //alert("Bd : " + this.Jobs[i].id + " :"  + data['jobid'])
                if(this.Jobs[i].id == data['jobid'])
                {
                    this.Jobs[i].bids.push(data);
                    this._Jobs.next(this.Jobs);
                }
            }
        });
    }

    getJobById(Id)
    {
        console.log("MyId : " + this.ServerUrl + '' + 'getJobById')
        let body = new FormData();
        body.append('id', Id);
        this.http.post(this.ServerUrl + '' + 'getJobById', body).map(res => res.json()).subscribe(data => {
            this.pushOpenJob(data);
        });
    }

    pushOpenJob(message) {
        console.log("OJ1 : " , this.OpenJobs)
        //this.OpenJobs.push(message);
        this.OpenJobs.splice(0,0,message);
        this._OpenJobs.next(this.OpenJobs);

        console.log("OJ2 : " , this.OpenJobs)
    }


    addJob(url: string, info: string) {
        let body = 'uid=' + window.localStorage.id + '&info=' + JSON.stringify(info);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res.json()).do((data) => {
            console.log("SaveMeals : ", data)
            this.Jobs = data;
            this._Jobs.next(this.Jobs);
        }).toPromise();
    }


    getAllJobs(url: string) {
        let body = new FormData();
        body.append('id', window.localStorage.id);

        // this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{
        //     console.log("ThisJobs : " , data)
        //     this.Jobs.next(data);
        // })
        console.log("All Jobs")

        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            //this.Jobs.next(data);
            console.log("AllJobs : " , data)
            this.Jobs = data;
            this._Jobs.next(this.Jobs);
        }).toPromise();
    }

    getBidById(url: string, id) {
        console.log("MyId : " , id)
        let body = new FormData();
        body.append('id', id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
        }).toPromise();
    }

    getAllOpenJobs(url: string) {
        let body = new FormData();
        body.append('id', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data) => {
            this.OpenJobs = data;
            this._OpenJobs.next(this.OpenJobs);
        }).toPromise();
    }

    UpdateJobState(url: string, id, state) {
        let body = new FormData();
        body.append('id', id);
        body.append('state', state);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data) => {
            console.log("getUserDetails : ", data)
        }).toPromise();
    }
};


