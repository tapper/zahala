import { NgModule } from '@angular/core';
import { AddJobModalComponent } from './add-job-modal/add-job-modal';
@NgModule({
	declarations: [AddJobModalComponent],
	imports: [],
	exports: [AddJobModalComponent]
})
export class ComponentsModule {}
