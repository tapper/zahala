import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AllSubstitutePage} from "../all-substitute/all-substitute";
import {MessagesPage} from "../messages/messages";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {Config} from "../../services/config";
import {loginService} from "../../services/loginService";
import {OpenJobsPage} from "../open-jobs/open-jobs";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    public UserType;
    public SelectedIndex:number = 2;
    tab1Root = AllSubstitutePage;
    tab2Root = UserJobsPage;
    tab3Root = MessagesPage;
    tab4Root = OpenJobsPage;

    constructor(public navCtrl: NavController, public Settings: Config, public Login: loginService) {
        console.log("11")

        if (!Settings.User) {
            this.Login.getUserById('getUserById').then(
                (data: any) => {
                    Settings.User = data[0];
                    this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
                    this.SelectedIndex = this.UserType == 2 ? 3 : 2;

                    console.log(this.UserType + " : "  + this.SelectedIndex)
                });
        }
    }
}
