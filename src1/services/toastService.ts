import { ToastController } from 'ionic-angular';
import {Injectable} from "@angular/core";

@Injectable()

export class toastService {

    constructor(private toastCtrl: ToastController) {console.log("shay")}

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'ToastClass'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }
}


